const user = {};

const group = {
  1: 3,
  2: 5,
  3: 2,
  4: 1,
  5: 4,
};

const promise1 = new Promise((resolve) => {
  setTimeout(() => resolve("Cherkasy"), 1000);
});

const promise2 = new Promise((resolve) => {
  setTimeout(() => resolve("admin"), 4000);
});

const promise3 = new Promise((resolve) => {
  setTimeout(() => resolve("1"), 2000);
});

function getNumberOfPeople(id) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(group[id]), 2000);
  });
}

const promise4 = new Promise((resolve) => {
  setTimeout(() => resolve(["Саша", "Влад", "Юля", "Андрей", "Богдан"]), 1000);
});

promise1
  .then((city) => {
    user.city = city;
    return promise2;
  })
  .then((name) => {
    user.email = `${name}.com`;
    if (user.email === 'admin.com') {
      return promise3;
    }
    return (new Error("You are not Admin"));
  })
  .then((id) => {
    user.id = id;
    return getNumberOfPeople(id);
  })
  .then((numberOfPeople) => {
    user.numberOfPeopleInGroup = numberOfPeople;
    return promise4;
  })
  .then((groupOfPeople) => {
    if (user.numberOfPeopleInGroup < groupOfPeople.length) {
      throw new Error('Too many people in group');
    } else {
      user.group = groupOfPeople;
    }
  });

function request(url) {
  return new Promise((res, rej) => {
    const delayTime = Math.floor(Math.random() * 10000) + 1;
    setTimeout(() => res(url), delayTime);
  });
}

function promiseAll(promises) {
  let counter = 0;
  const result = [];
  return new Promise((resolve, reject) => {
    promises.forEach((promise, index) => {
      promise.then((value) => {
        result[index] = value;
        counter += 1;
        if (promises.length === counter) {
          resolve(result);
        }
      }).catch((error) => {
        reject(error);
      });
    });
  });
}