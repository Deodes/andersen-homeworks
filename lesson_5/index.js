class WeatherForecast {
    API_KEY = '46522a888100f74fa4c6e382eb1ff0f8';

    constructor(city) {
        this.city = city;
    }

    getData() {
        return fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${this.city}&appid=${this.API_KEY}`)
    }

}

class CityWeatherForecast extends WeatherForecast {
    constructor(city) {
        super(city);
    }

    nearFutureForecast() {
        super.getData()
            .then((response) => response.json())
            .then((weatherForecast) => resolve(weatherForecast.list[0]))
    }
}

class RandomCityWeatherForecast extends WeatherForecast {
    constructor(arrayOfCities) {
        super();
        this.cities = arrayOfCities;
    }

    getData() {
        const RANDOM_NUMBER = Math.round(Math.random() * (this.cities.length - 1));
        return fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${this.cities[RANDOM_NUMBER]}&appid=${this.API_KEY}`)
    }

    getCityName(){
        this.getData()
            .then((response) => response.json())
            .then((randomCityInfo) => resolve(randomCityInfo.city.name))
    }
}

const odessa = new CityWeatherForecast('Odessa');
const randomCity = new RandomCityWeatherForecast(['Berlin', 'London', 'Kiev']);



