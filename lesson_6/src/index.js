function createCalculator() {
    const NUMBER_OF_BUTTONS = 18;
    const buttons = ['AC', '←', '/', '7', '8', '9', '*', '4', '5', '6', '-', '1', '2', '3', '+', '0', '.', '='];
    const KEY = 'Last expression';

    const container = document.createElement('div');
    const wrapper = document.createElement('div');
    const screen = document.createElement('div');
    const form = document.createElement('form');
    const input = document.createElement('input');
    const ul = document.createElement('ul');

    form.setAttribute('type', 'text');
    form.setAttribute('name', 'text');
    input.setAttribute('readonly', '');

    container.style.cssText = 'display: flex; justify-content: space-between; max-width: 980px; padding: 10px; margin: 0 auto;';
    wrapper.style.cssText = `max-width: 360px; display: grid; grid-template-columns: repeat(4, 1fr); grid-gap: 4px; 
    padding: 4px; border: 2px solid black; background-color: #4682B4;`;
    screen.style.cssText = 'display: flex; justify-content: center; align-items: center; grid-column: 1/-1;'
    form.style.cssText = 'max-width: 100%;';
    input.style.cssText = 'width: 100%; font-size: 30px; outline: none; text-align: right;';
    ul.style.cssText = 'list-style: none;';



    form.append(input);
    screen.append(form);
    wrapper.append(screen);
    container.append(wrapper);
    container.append(ul);

    for (let i = 0; i < NUMBER_OF_BUTTONS; i++) {

        const button = document.createElement('button');
        button.style.padding = '20px';
        button.textContent = buttons[i];
        wrapper.append(button);
    }

    wrapper.children[1].style.gridColumn = '1/3';
    wrapper.children[16].style.gridColumn = '1/3';

    wrapper.children[1].addEventListener('click', event => {
        event.preventDefault();
        input.value = '';
    })
    
    wrapper.children[2].addEventListener('click', event => {
        event.preventDefault();
        const SUBSTRING = input.value;
        input.value = SUBSTRING.substring(0, SUBSTRING.length - 1);
    })

    for (let i = 3; i <= NUMBER_OF_BUTTONS-1; i++){
        wrapper.children[i].addEventListener('click', event => {
            event.preventDefault();
            input.value += buttons[i-1];
        })
    }

    wrapper.children[18].addEventListener('click', event => {
        event.preventDefault();

        const li = document.createElement('li');

        li.textContent = input.value;

        const calculate = new Function(`return ${input.value}`);
        input.value = calculate();

        li.textContent += `=${input.value}`;

        li.style.cssText = 'font-size:26px; margin-bottom: 15px; text-align: right';

        localStorage.setItem(KEY, li.textContent);

        ul.append(li);
    })

    document.body.append(container);
}

createCalculator();

