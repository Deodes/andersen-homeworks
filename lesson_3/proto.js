function Grid(rows, cols, tableName, color, borderSize) {
    this.rows = rows;
    this.cols = cols;
    this.tableName = tableName;
    this.color = color;
    this.borderSize = borderSize;
    this.rowsValue = [];
    this.IsFullTransparently = false;
  };
  
  Grid.prototype.changeBorderSize = function (newSize) {
    this.borderSize = newSize;
  };
  
  Grid.prototype.changeColor = function (newColor) {
    this.color = newColor;
  };
  
  Grid.prototype.changeTableName = function (newName) {
    this.tableName = newName;
  };
  
  Grid.prototype.addInfo = function (info) {
    this.info = info;
  };
  
  Grid.prototype.changeTransparentMode = function (mode) {
    this.IsFullTransparently = mode;
  }
  
  function User(rows, cols, tableName, color, borderSize) {
    Grid.call(this, rows, cols, tableName, color, borderSize);
  };

  User.prototype = Object.create(Grid.prototype); 
  
  User.prototype.deleteUser = function (id) {
    this.rowsValue = this.rowsValue.filter((rowValue) => rowValue.id !== id);
  };
  
  User.prototype.changeName = function (id, newName) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      name: rowValue.id === id ? newName : rowValue.name,
    }));
  };
  
  User.prototype.changeColor = function (color) {
    Grid.prototype.changeColor.call(this, color);
    Grid.prototype.changeTransparentMode.call(this, true);
  };
  
  User.prototype.addInfo = function (name, surname, age) {
    if (this.rowsValue.length >= this.rows) {
      throw new Error('U need more rows');
    } else if (arguments.length + 1 > this.cols) {
      throw new Error('You need more columns');
    } else {
      this.rowsValue.push({
        id: this.rowsValue.length + 1,
        name,
        surname,
        age,
      });
    }
  };
  
  User.prototype.getTheBiggestAge = function () {
    const BIGGEST_NUMBER = this.rowsValue.reduce((acc, rowValue) => {
      if (acc <= rowValue.age) {
        acc = rowValue.age;
      }
      return acc;
    }, 0);
    return BIGGEST_NUMBER;
  };
  
  function Order(rows, cols, tableName, color, borderSize) {
    Grid.call(this, rows, cols, tableName, color, borderSize);
    this.isPrivate = true;
  };

  Order.prototype = Object.create(Grid.prototype); 
  
  Order.prototype.addInfo = function (price, country, city) {
    if (this.rowsValue.length >= this.rows) {
      throw new Error('You need more rows');
    } else if (arguments.length + 1 > this.cols) {
      throw new Error('You need more columns');
    } else {
      this.rowsValue.push({
        id: this.rowsValue.length + 1,
        price,
        country,
        city,
      });
    }
  };
  
  Order.prototype.convertToGrivna = function (id) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      price: rowValue.id === id ? rowValue.price * 28 : rowValue.price,
    }));
  };
  
  Order.prototype.changeCountry = function (id, newCountry) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      country: rowValue.id === id ? newCountry : rowValue.country,
    }));
  };
  
  Order.prototype.changeCity = function (id, newCity) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      city: rowValue.id === id ? newCity : rowValue.city,
    }));
  };
  
  Order.prototype.switchSecurityMode = function () {
    this.isPrivate = !(this.isPrivate);
  };
  
  const user1 = new User(3, 4, 'Admins', 'green', '1px');
  const order1 = new Order(3, 4, 'Autos', 'yellow', '2px');
