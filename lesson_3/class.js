class Grid {
  constructor(rows, cols, tableName, color, borderSize) {
    this.rows = rows;
    this.cols = cols;
    this.tableName = tableName;
    this.color = color;
    this.borderSize = borderSize;
    this.rowsValue = [];
    this.IsFullTransparently = false;
  };

  changeBorderSize(newSize) {
    this.borderSize = newSize;
  };

  changeColor(newColor) {
    this.color = newColor;
  };

  changeTableName(newName) {
    this.tableName = newName;
  };

  addInfo(info) {
    this.info = info;
  };

  changeTransparentMode(mode) {
    this.IsFullTransparently = mode;
  };
}

class User extends Grid {
  addInfo(name, surname, age) {
    if (this.rowsValue.length >= this.rows) {
      throw new Error('U need more rows');
    } else if (arguments.length + 1 > this.cols) {
      throw new Error('You need more columns');
    } else {
      this.rowsValue.push({
        id: this.rowsValue.length + 1,
        name,
        surname,
        age,
      });
    }
  };

  deleteUser(id) {
    this.rowsValue = this.rowsValue.filter((rowValue) => rowValue.id !== id);
  };

  changeName(id, newName) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      name: rowValue.id === id ? newName : rowValue.name,
    }));
  };

  changeColor(color) {
    super.changeColor.call(this, color);
    super.changeTransparentMode(true);
  };
}

class Order extends Grid {
  constructor(rows, cols, tableName, color, borderSize) {
    super(rows, cols, tableName, color, borderSize);
    this.isPrivate = true;
  };

  addInfo(price, country, city) {
    if (this.rowsValue.length <= this.rows) {
      throw new Error('You need more rows');
    } else if (arguments.length + 1 > this.cols) {
      throw new Error('You need more columns');
    } else {
      this.rowsValue.push({
        id: this.rowsValue.length + 1,
        price,
        country,
        city,
      });
    }
  };

  convertToGrivna(id) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      price: rowValue.id === id ? rowValue.price * 28 : rowValue.price,
    }));
  };

  changeCountry(id, newCountry) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      country: rowValue.id === id ? newCountry : rowValue.country,
    }));
  };

  changeCity(id, newCity) {
    this.rowsValue = this.rowsValue.map((rowValue) => ({
      ...rowValue,
      city: rowValue.id === id ? newCity : rowValue.city,
    }));
  };

  switchSecurityMode() {
    this.isPrivate = !(this.isPrivate);
  };
}


const user1 = new User(5, 4, 'first', 'grey', '2px');
const order1 = new Order(3, 3, 'first', 'green', '3px');
