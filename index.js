const myObj = { 
  name: 'Vladimir',
  surname: 'Penchev',
  age: 20,
  city: "Odessa",
  hobbies: "codding and play basketball",
  aboutMe() {
    return `Hello there, my fullname is ${this.name} ${this.surname}, I'm ${this.age} years old. Living in ${this.city} city. I like ${this.hobbies}.`
  }
}
